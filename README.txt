
Nodecomment Subcriptions
=======================================


DESCRIPTION
-----------
This Modules allows Users to subscribe to comments of nodes which have the module Nodecomments enabled.

Actually there is the Nodecomment Notify Module, but this does allow you only to subscribe with a new comment. And if you are using the Subscriptions module for other nodes already on your Drupal site it is a lot better to provide your users a single place where they can manage all the different subscriptions.


REQUIREMENTS
------------
Drupal 6.x
Nodecomment (and all his dependencies) 
Subscriptions (and all his dependencies) 


INSTALLING
----------
1. To install the module copy the 'subscriptions_nodecomment' folder to your sites/all/modules directory.

2. Go to admin/build/modules. Enable the module.


CONFIGURING AND USING
---------------------
To use this module you need to enable the subscription functionality to the Node which has the Nodecomments enabled, not the comments node itself. It then adds a new subscription checkbox "Subscribe to this page and his comments".
